<resources>
    <string name="app_name" translatable="false">VLC Benchmark</string>
    <string name="upload_results">Upload</string>
    <string name="default_percent_value">0 %</string>
    <string name="no_results">No result found, please run a test.</string>

    <!-- page names -->
    <string name="settings_page">Settings</string>
    <string name="results_page">Benchmark Results</string>

    <!-- bottom_navigation_bar -->
    <string name="home_nav">Home</string>
    <string name="result_nav">Results</string>
    <string name="settings_nav">Settings</string>

    <!-- toolbar titles -->
    <string name="title_home">VLC Benchmark</string>
    <string name="title_results">Results</string>
    <string name="title_settings">Settings</string>

    <!-- settings -->
    <string name="general_pref_category">General</string>
    <string name="misc_pref_category">Misc</string>
    <string name="storage_pref_category">Storage</string>
    <string name="debug_pref_category">Debug</string>
    <string name="about_pref">About</string>
    <string name="delete_saves_pref">Delete benchmarks</string>
    <string name="delete_saves_key">delete_saves_key</string>
    <string name="delete_samples_pref">Delete video samples</string>
    <string name="delete_samples_key">delete_samples_key</string>
    <string name="connect_pref">Connect to Google account</string>
    <string name="connect_key">connect_key</string>
    <string name="disconnect_pref">Disconnect: </string>
    <string name="disconnect_key">disconnect_key</string>
    <string name="storage_pref">File location</string>
    <string name="storage_key">storage_key</string>
    <string name="internal_memory">Internal Memory</string>
    <string name="sdcard">SD Card: %1$s</string>
    <string name="screenshot_upload_pref">Upload screenshots</string>
    <string name="screenshot_upload_key">screenshot_upload_key</string>
    <string name="screenshot_upload_summary">This is only for debug purposes, check this only if you are in contact with the dev team concerning screenshot problems</string>
    <string name="screenshot_upload_all_pref">Upload all screenshots</string>
    <string name="screenshot_upload_all_key">screenshot_upload_all_key</string>
    <string name="screenshot_upload_all_summary">This is only for debug purposes, upload all screenshots not just faulty ones</string>
    <string name="vlc_failure_stacktrace_pref">VLC Android failure stacktrace</string>
    <string name="vlc_failure_stacktrace_key">stacktrace_failure_key</string>
    <string name="vlc_failure_stacktrace_summary">This is only for debug purposes, check this only if you are in contact with the dev team concerning VLC For Android failures in the results. This will keep stack traces from VLC For Android to help understand failures. May contain personnal information.</string>

    <!-- About content -->
    <string name="about_description">VLCBenchmark is a tool to benchmark android devices\' video capabilities using the VLC Media Player.</string>
    <string name="about_copyright" translatable="false">Copyleft &#169; 2016&#8211;2021 by VideoLAN.</string>
    <string name="about_authors" translatable="false">Jean-Baptiste&#160;Kempf, Geoffrey&#160;Métais, Alexandre&#160;Perraud, Thomas&#160;Guillem, Benoit&#160;Du&#160;Payrat, Bastien&#160;Penavayre, Duncan&#160;McNamara</string>
    <string name="about_revision">Revision:</string>
    <string name="about_compiled_by">This VLCBenchmark version is compiled by:</string>
    <string name="about_link">https://www.videolan.org</string>
    <string name="about_bench_link">https://bench.videolabs.io</string>
    <string name="about_vlc_min">VLC version minimum: %1$s</string>
    <!-- About tabs -->
    <string name="tab_about">About</string>
    <string name="tab_licence">Licence</string>

    <!-- Benchmark Activity -->
    <string name="download_title">Sample Download</string>
    <string name="download_check_title">Sample Check</string>
    <string name="benchmark_title">Benchmark</string>
    <string name="upload_title">Upload Results</string>
    <string name="stepper_no_samples_text">There was a problem while fetching the benchmark sample list. Make sure you are connected to the internet before trying again.</string>
    <string name="stepper_no_space_text">You are missing %1$s on your device. Please make room before trying again.</string>
    <string name="stepper_error">There was a interruption to the benchmark process. Do you want to start again ?</string>
    <string name="stepper_no_google_text">Your device does not have up to date google services.\nBecause of this you won\'t be able to upload the results of this benchmark.\n\nDo you want to continue ?</string>

    <!-- dialog titles -->
    <string name="dialog_title_error">Error</string>
    <string name="dialog_title_warning">Warning</string>
    <string name="dialog_title_oups">Oups &#8230;</string>
    <string name="dialog_title_missing_vlc">Missing VLC</string>
    <string name="dialog_title_outdated_vlc">Outdated VLC</string>
    <string name="dialog_title_testing">Testing &#8230;</string>
    <string name="dialog_title_file_deletion">File deletion</string>
    <string name="dialog_title_sample_deletion">Sample deletion</string>
    <string name="dialog_title_downloading">Preparing files &#8230;</string>
    <string name="dialog_title_checking_files">Checking files</string>
    <string name="dialog_title_success">Success</string>
    <string name="dialog_title_previous_bench">Previous benchmark</string>
    <string name="dialog_title_hello">Hello</string>
    <string name="dialog_title_upload_results">Uploading results &#8230;</string>
    <string name="dialog_title_result_upload">Result upload</string>
    <string name="dialog_title_file_copy">Copying files</string>

    <!-- dialog texts -->
    <string name="dialog_text_sample">There was a problem while checking samples</string>
    <string name="dialog_text_err_google">There was a problem while connecting to Google</string>
    <string name="dialog_text_err_upload">There was a problem while uploading the results.</string>
    <string name="dialog_text_upload_success">Thank you for uploading your results.\n\nYou can check them out and others on our website.</string>
    <string name="dialog_text_downloading">Downloading &#8230;</string>
    <string name="dialog_text_testing">Testing &#8230;</string>
    <string name="dialog_text_download_error">There was an error during download</string>
    <string name="dialog_text_missing_vlc">You need the latest VLC Media Player to start a benchmark (VLC-Android %1$s).\nPlease install it to continue.</string>
    <string name="dialog_text_missing_vlc_beta">This is a beta, and you need the latest VLC Android Beta to start a benchmark (VLC-Android %1$s).\nPlease register to the VLC Android Beta program to install it. </string>
    <string name="dialog_text_oups">There was an unexpected problem</string>
    <string name="dialog_text_battery_warning">You only have %1$d %% of battery charge left, you should plug your phone</string>
    <string name="dialog_text_file_deletion_success">Deleted all benchmark results</string>
    <string name="dialog_text_samples_deletion_success">Deleted all samples</string>
    <string name="dialog_text_file_deletion_failure">Failed to delete all benchmark results</string>
    <string name="dialog_text_sample_deletion_success">Deleted all test samples</string>
    <string name="dialog_text_sample_deletion_failure">Failed to delete all test samples</string>
    <string name="dialog_text_deletion_confirmation">Are you sure you want to delete these files ?</string>
    <string name="dialog_text_save_failure">There was a problem while saving the benchmark results.</string>
    <string name="dialog_text_file_creation_failure">There was a problem while creating files</string>
    <string name="dialog_text_loading_results_failure">There was a problem while loading results</string>
    <string name="dialog_text_saving_results_failure">Failed to save benchmark results</string>
    <string name="dialog_text_no_wifi">You are not connected to wifi</string>
    <string name="dialog_text_invalid_file">This file is corrupted.\nPlease restart the download to replace it.</string>
    <string name="dialog_text_vlc_failed">Failed to start VLC.</string>
    <string name="dialog_text_no_touch_warning">The benchmark is about to start, and will now alternate between VLC Android and the benchmark app. During the VLC Android phases, the interface will not be responsive. Do you want to continue ?</string>
    <string name="dialog_text_no_touch_warning_3">The triple benchmark is about to start, it will take a lot of time. It will now alternate between VLC Android and the benchmark app. During the VLC Android phases, the interface will not be responsive. Do you want to continue ?</string>
    <string name="dialog_text_missing_space">Can\'t download files, you are missing %1$s of free space</string>
    <string name="dialog_text_previous_bench">There is a previous benchmark that was interrupted at test %1$d / %2$d. Do you want to continue or discard it?</string>
    <string name="dialog_text_previous_bench_loops">There is a previous benchmark that was interrupted at loop %1$d / %2$d and test %3$d / %4$d. Do you want to continue or discard it</string>
    <string name="dialog_text_error_connect">Could not connect to our database. Please try again later.</string>
    <string name="dialog_text_error_config">There was an error loading the benchmark configuration.</string>
    <string name="dialog_text_error_io">There was a problem while storing the downloaded media.</string>
    <string name="dialog_text_error_conf">There was a problem fetching configuration files.</string>
    <string name="dialog_text_initial_warning">VLCBenchmark will extensively test your phone\'s video capabilities.\n\nIt will download a large amount of files and will run for several hours.\nFurthermore, it will need the permission to access external storage"</string>
    <string name="dialog_text_error_permission">This application can\'t operate without the requested permissions</string>
    <string name="dialog_text_download_warning">You are about to download %1$s of samples. Do you want to continue ?</string>
    <string name="dialog_text_no_wifi_download_warning">You are about to download %1$s of samples. This is very costly for your mobile data plan. Do you want to continue ?</string>
    <string name="dialog_text_download_progress">%1$s %% (%2$s)\n%3$s / %4$s</string>
    <string name="dialog_text_download_downloading">Downloading: </string>The benchmark is about to start, and will now alternate between VLC Android and the benchmark app. During the VLC Android phases, the interface w
    <string name="dialog_text_download_checking_file">Checking file integrity: </string>
    <string name="dialog_text_result_upload">Help us improve the benchmark results by uploading the screenshots taken during testing. This upload is %1$s in size, only upload if you have a fast connection.</string>
    <string name="dialog_text_android_10">Due to limitations in Android 10, the benchmark cannot run on this device. We apologise for the inconvenience.</string>
    <string name="dialog_text_confirm_bench_delete">Are you sure you want to delete this benchmark ?</string>
    <string name="dialog_text_sample_free_space">Do you want to delete the media samples, and free some space, now that the benchmark is done ?</string>
    <string name="dialog_text_upload_explanation">Thank you for running this benchmark. Do you want to upload the results online ? If so it will be required that you authenticate with your google account</string>

    <!-- Progress -->
    <string name="progress_download_complete">100%\nDownload Complete</string>

    <!-- Notifications -->
    <string name="notif_download_content_title">VLCBenchmark Sample Download</string>
    <string name="notif_benchmark_content_title">VLCBenchmark</string>
    <string name="notif_transfert_content_title">VLCBenchmark Sample Transfer</string>
    <string name="notif_benchmark_channel_description">VLCBenchmark Service channel</string>
    <string name="notif_transfert_channel_description">VLCBenchmark Sample Transfer channel</string>
    <string name="notif_benchmark_content_text">Touch to resume VLCBenchmark</string>

    <!-- Toasts -->
    <string name="toast_text_error_prep_upload">There was an error while preparing the upload</string>
    <string name="toast_error_mountpoint_no_space">There isn\'t enough space in the new storage location</string>
    <string name="toast_error_file_transfert">There was a problem while transferring files</string>
    <string name="toast_error_result_detail_error">Failed to display result details</string>
    <string name="toast_error_result_parse">Failed to save results</string>
    <string name="toast_error_fail_load_config">Failed to load test configuration</string>

    <!-- Snackbar -->
    <string name="snack_error_failed_start_vlc">Failed to start VLC-Android</string>
    <string name="snack_error_screenshot_preparation">Failed in screenshot preparation</string>
    <string name="snack_error_screenshot_projection">Failed to get screenshot projection</string>
    <string name="snack_error_vlc_screenshot">Failed to take screenshots in vlc-android</string>
    <string name="snack_error_file_download_fail">Failed to download file</string>
    <string name="snack_error_invalid_file">Invalid file, please re-download it</string>
    <string name="snack_error_no_samples">Failed to get video samples</string>
    <string name="snack_error_no_browser_googleplay">No Google Play or web browser</string>
    <string name="snack_error_invalid_image_dimensions">Failed to get positive image dimensions</string>

    <!-- Errors -->
    <string name="error_service_fail_start">Failed to start service</string>

    <!-- dialog buttons -->
    <string name="dialog_btn_continue">Continue</string>
    <string name="dialog_btn_not_now">Not now</string>
    <string name="dialog_btn_cancel">Cancel</string>
    <string name="dialog_btn_ok">OK</string>
    <string name="dialog_btn_restart">Restart</string>
    <string name="dialog_btn_done">Done</string>
    <string name="dialog_btn_visit">Visit</string>
    <string name="dialog_btn_upload_with">Upload with screenshots</string>
    <string name="dialog_btn_upload_without">Upload without screenshots</string>
    <string name="dialog_btn_yes">Yes</string>
    <string name="dialog_btn_no">No</string>
    <string name="dialog_btn_try_again">Try Again</string>

    <!-- Notification buttons -->
    <string name="notification_btn_close">Close</string>

    <!-- main page -->
    <string name="specs_title">DEVICE</string>
    <string name="specs_model">Model</string>
    <string name="specs_android">Android</string>
    <string name="specs_cpu">CPU</string>
    <string name="specs_cpuspeed">CPU Speed</string>
    <string name="specs_memory">Memory</string>
    <string name="specs_resolution">Resolution</string>
    <string name="specs_free_space">Free space</string>
    <string name="explanation_title">BENCHMARK</string>
    <string name="explanation_text">
        This benchmark will test your device\'s video capabilities using the VLC Media Player engine.\nYou can start a benchmark with the button below, or start 3 benchmarks and get the average results with a long click.
    </string>
    <string name="specs_cpu_speed_value">%1$s - %2$s</string>

    <string name="missing_samples">Missing test samples.</string>
    <string name="download_warning">
        To perform a benchmark, some test samples need to be downloaded.\nYou need to be connected to the wifi in order to download them.\nPlease note it could take some time and storage space, as there may be many.\nFurthermore, the said list is likely to evolve.
    </string>

    <string name="dialog_text_starting">Starting benchmark</string>
    <string name="progress_text_percent">%1$s %%</string>
    <string name="progress_text_format">Test: %1$d/%2$d\nTest mode: %3$s</string>
    <string name="progress_text_format_loop">Test: %1$d/%2$d\nTest mode: %3$s\nLoop %4$d/%5$d</string>
    <string name="dialog_text_file_check_progress">File: %1$d/%2$d </string>


    <!-- result list -->
    <string name="result_score">Score: </string>
    <string name="result_score_value">%1$s</string>
    <string name="individual_test_score">%1$s / %2$s</string>
    <string name="software_score">Software Score</string>
    <string name="hardware_score">Hardware Score</string>
    <string name="score">%1$d/%2$d </string>
    <string name="simple_score">%1$d</string>
    <string name="simple_decimal_str">%1$s</string>
    <string name="simple_percent">%1$d%%</string>
    <string name="decimal_percent">%1$s%%</string>
    <string name="software_title">Software Decoding </string>
    <string name="hardware_title">Hardware Decoding </string>
    <string name="playback_score">Playback score </string>
    <string name="quality_score">Quality score </string>
    <string name="speed_score">Speed Score </string>
    <string name="frames_dropped">Frames dropped</string>
    <string name="frames_dropped_percentage">Percentage of frames dropped</string>
    <string name="warnings">Warnings</string>
    <string name="crash">Crash: %1$s</string>
    <string name="bad_screenshots">Bad screenshots</string>
    <string name="speed_speed">Speed</string>

    <!-- VLC Results string equivalence-->
    <string name="result_canceled">VLC has crashed</string>
    <string name="result_no_hw">No hardware decoder</string>
    <string name="result_connection_failed">Connection failed to audio service</string>
    <string name="result_playback_error">VLC is not able to play this file, it could be incorrect path/uri, not supported codec or broken file"</string>
    <string name="result_hardware_acceleration_error">Error with hardware acceleration, user refused to switch to software decoding</string>
    <string name="result_video_track_lost">VLC continues playback, but for audio track only. (Audio file detected or user chose to)</string>
    <string name="result_vlc_crash">VLC Benchmark has crashed</string>
    <string name="result_unknown">Unknown problem</string>
    <string name="result_no_stacktrack_perm">"No permission for stacktraces"</string>

    <!-- Onboarding -->
    <string name="onboarding_title_intro">Welcome</string>
    <string name="onboarding_img_intro">Onboarding introduction icon</string>
    <string name="onboarding_text_intro">VLCBenchmark will extensively test your phone\'s video capabilities by playing many video samples using VLC Android\n\nIt is better to use a new device for this, as an old one would see its performance impacted by use.</string>
    <string name="onboarding_title_download">File download</string>
    <string name="onboarding_img_download">Onboarding download icon</string>
    <string name="onboarding_text_download">To run tests, VLC Benchmark will need to download a large amount of files (approximately 5GB). Because of that, we have to request Read and Write permission.</string>
    <string name="onboarding_title_warnings">Warning</string>
    <string name="onboarding_img_warnings">Onboarding warnings icon</string>
    <string name="onboarding_text_warnings">This benchmark takes around 4h to run because of the amount of samples to test. You should to keep you phone connected to a power supply\n\nThis benchmark may cause your telephone to reboot.</string>
    <string name="onboarding_btn_previous">Previous button</string>
    <string name="onboarding_btn_next">Next button</string>

    <string name="title_activity_splash">SplashActivity</string>

    <!-- Size units -->
    <string name="size_unit_kilo">KB</string>
    <string name="size_unit_mega">MB</string>
    <string name="size_unit_giga">GB</string>
    <string name="size_unit_per_second_byte"> Bps</string>
    <string name="size_unit_per_second_kilo">KBps</string>
    <string name="size_unit_per_second_mega">MBps</string>
    <string name="size_unit_per_second_giga">GBps</string>

</resources>
