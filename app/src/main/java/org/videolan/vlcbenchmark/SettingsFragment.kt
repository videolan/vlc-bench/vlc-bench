/*
 *****************************************************************************
 * SettingsFragment.kt
 *****************************************************************************
 * Copyright © 2017 - 2021 VLC authors and VideoLAN
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/
package org.videolan.vlcbenchmark

import android.content.*
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.*
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import org.videolan.vlcbenchmark.tests.TestRepository.deleteFiles
import org.videolan.vlcbenchmark.tools.*
import org.videolan.vlcbenchmark.tools.StorageManager.EXTERNAL_PUBLIC_DIRECTORY
import org.videolan.vlcbenchmark.tools.StorageManager.baseDir
import org.videolan.vlcbenchmark.tools.StorageManager.checkNewMountpointFreeSpace
import org.videolan.vlcbenchmark.tools.StorageManager.directory
import org.videolan.vlcbenchmark.tools.StorageManager.externalStorageDirectories
import org.videolan.vlcbenchmark.tools.StorageManager.getDirectoryMemoryUsage
import org.videolan.vlcbenchmark.tools.StorageManager.getFilenameFromPath
import org.videolan.vlcbenchmark.tools.StorageManager.getInternalDirStr
import org.videolan.vlcbenchmark.tools.StorageManager.mountpoint
import org.videolan.vlcbenchmark.tools.Util.registerReceiverCompat
import java.io.File
import java.util.*

class SettingsFragment : PreferenceFragmentCompat() {
    private lateinit var googleConnectionHandler: GoogleConnectionHandler
    private var progressBottomSheet: ProgressBottomSheet? = null
    private var copySize = -1L
    private var backToTop = false
    private var onScrollListener: RecyclerView.OnScrollListener? = null
    private var br = SettingsFragmentBroadcastReceiver()

    private var service: SampleTransferService? = null
    var bound = false


    fun scrollingBackToTop() {
        listView?.let {
            backToTop = true
            it.scrollToPosition(0)
            // if the position is already 0, then the following condition will be true
            // if not, we must wait for the scroll to take effect before accessing the first
            // element in the listView. This is handled in the scrollListener set in onResume()
            if (it.findViewHolderForLayoutPosition(1) != null) {
                it.findViewHolderForLayoutPosition(1)?.itemView?.requestFocus()
                backToTop = false
            }
        }
    }

    fun setSelectionToTop() {
        backToTop = false
        this.listView.findViewHolderForLayoutPosition(1)?.itemView?.requestFocus()
    }

    private val connection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(componentName: ComponentName, iBinder: IBinder) {
            val binder = iBinder as SampleTransferService.SampleTransferServiceBinder
            service = binder.getService()
            bound = true
        }

        override fun onServiceDisconnected(componentName: ComponentName) {
            bound = false
        }
    }

    override fun onResume() {
        super.onResume()
        googleConnectionHandler = GoogleConnectionHandler.instance
        googleConnectionHandler.setGoogleSignInClient(requireContext(), requireActivity())
        updateGoogleButton()
        onScrollListener = object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (backToTop && dx == 0 && dy == 0) {
                    setSelectionToTop()
                }
            }
        }
        this.listView.addOnScrollListener(onScrollListener as RecyclerView.OnScrollListener)
        val preference = findPreference<ListPreference>("storage_key")
        if (activity != null && preference != null) {
            preference.title = getString(R.string.storage_pref)
            setStoragePreference(preference, null)
            val entries = ArrayList<String>()
            entries.add(getString(R.string.internal_memory))
            for (value in externalStorageDirectories) {
                entries.add(String.format(getString(R.string.sdcard), value))
            }
            preference.entries = entries.toTypedArray<CharSequence>()
            val entryValues = ArrayList<String>()
            entryValues.add(EXTERNAL_PUBLIC_DIRECTORY)
            entryValues.addAll(externalStorageDirectories)
            preference.entryValues = entryValues.toTypedArray<CharSequence>()
            preference.onPreferenceChangeListener = Preference.OnPreferenceChangeListener { _: Preference?, _newValue: Any ->
                val oldValue = directory
                val newValue = _newValue as String + baseDir
                if (oldValue != null && checkNewMountpointFreeSpace(oldValue, newValue)) {
                    copySize = getDirectoryMemoryUsage(oldValue)
                    val intent = Intent(activity!!, SampleTransferService::class.java)
                    intent.action = Constants.ACTION_START_SAMPLE_TRANSFER
                    intent.putExtra(Constants.EXTRA_SAMPLE_TRANSFER_OLD_DIR, oldValue)
                    intent.putExtra(Constants.EXTRA_SAMPLE_TRANSFER_NEW_DIR, newValue)
                    bindToService(intent)
                    createDialog()

                    true
                } else {
                    view?.let {
                        Snackbar.make(it, R.string.toast_error_mountpoint_no_space, Snackbar.LENGTH_LONG).show()
                    }
                    false
                }
            }
            val filter = IntentFilter(Constants.ACTION_FINISHED_SAMPLE_TRANSFER)
            filter.addAction(Constants.ACTION_SERVICE_ERROR)
            filter.addAction(Constants.ACTION_UPDATE_PROGRESS)
            requireActivity().registerReceiverCompat(br, filter, false)
        }
    }

    private fun bindToService(intent: Intent) {
        requireActivity().startService(intent)
        val boundIntent = Intent(activity!!, SampleTransferService::class.java)
        requireActivity().bindService(boundIntent, connection, AppCompatActivity.BIND_AUTO_CREATE)
    }

    private fun unbindFromService() {
        if (bound) {
            requireActivity().unbindService(connection)
            bound = false
        }
    }

    private fun stopService() {
        service?.let {
            it.stopSelf()
            unbindFromService()
        }
    }

    override fun onStop() {
        super.onStop()
        stopService()
    }

    private fun createDialog() {
        if (activity != null) {
            progressBottomSheet = ProgressBottomSheet()
            progressBottomSheet?.setTitle(R.string.dialog_title_file_copy)
            progressBottomSheet?.setCancelCallback { onDialogCancel() }
            progressBottomSheet?.isCancelable = false
            progressBottomSheet?.show(requireActivity().supportFragmentManager, "Sample Tranfert BottomSheet")
        }
    }

    inner class SettingsFragmentBroadcastReceiver: BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            intent?.let {
                when (intent.action) {
                    Constants.ACTION_SERVICE_ERROR -> {
                        val errorStringId = intent.getIntExtra(Constants.EXTRA_SERVICE_ERROR, 0)
                        if (errorStringId != 0) {
                            view?.let {
                                Snackbar.make(it, errorStringId, Snackbar.LENGTH_LONG).show()
                            }
                        }
                        progressBottomSheet?.dismiss()
                        stopService()
                    }
                    Constants.ACTION_UPDATE_PROGRESS -> {
                        val percent = intent.getDoubleExtra(Constants.EXTRA_DOWNLOAD_PERCENT, 0.0)
                        val progressString = intent.getStringExtra(Constants.EXTRA_DOWNLOAD_STRING) ?: ""
                        updateProgress(percent, progressString)
                    }
                    Constants.ACTION_FINISHED_SAMPLE_TRANSFER -> {
                        val newDir = intent.getStringExtra(Constants.EXTRA_SAMPLE_TRANSFER_NEW_DIR) ?: ""
                        stopService()
                        onFileCopied(newDir)
                    }
                    else -> {}
                }
            }
        }
    }

    private fun onDialogCancel() {
        requireActivity().sendBroadcast(Intent(Constants.ACTION_CANCEL_SAMPLE_TRANSFERT))
    }

    fun onFileCopied(newValue: String?) {
        var value = newValue
        if (value == null) {
            Log.e(TAG, "onFileCopied: new value is null")
            return
        }
        value = value.replace(baseDir, "")
        if (activity != null) {
            val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(requireContext())
            requireActivity().requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_FULL_USER
            val preference = findPreference<ListPreference>("storage_key")
            if (preference != null) {
                val editor = sharedPreferences.edit()
                editor.putString("storage_dir", value)
                editor.apply()
                setStoragePreference(preference, value)
            }
        }
        stopService()
        progressBottomSheet?.dismiss()
        progressBottomSheet = null
        copySize = -1L
    }

    fun updateProgress(percent: Double, progressString: String) {
        Util.runInUiThread {
            progressBottomSheet?.updateProgress(percent, progressString)
        }
    }

    private fun setStoragePreference(preference: ListPreference, location: String?) {
        var newLocation = location
        if (newLocation == null) newLocation = mountpoint
        preference.value = newLocation
        val subtitle: String = if (EXTERNAL_PUBLIC_DIRECTORY == newLocation) {
            getString(R.string.internal_memory)
        } else {
            if (activity != null) {
                String.format(getString(R.string.sdcard),
                        getFilenameFromPath(newLocation))
            } else {
                getFilenameFromPath(newLocation)
            }
        }
        preference.summary = subtitle
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        if (BuildConfig.DEBUG) {
            addPreferencesFromResource(R.xml.app_debug_preferences)
        } else {
            addPreferencesFromResource(R.xml.app_preferences)
        }
    }

    private fun deleteSamples() {
        val dialog = DialogInstance(
                R.string.dialog_title_sample_deletion, R.string.dialog_text_samples_deletion_success)
        val dirPath = getInternalDirStr(StorageManager.mediaFolder)
        if (dirPath == null) {
            Log.e(TAG, "deleteSamples: media folder path is null")
            return
        }
        val dir = File(dirPath)
        val files = dir.listFiles()
        if (files != null) {
            for (file in files) {
                if (!file.delete()) {
                    Log.e(TAG, "Failed to delete sample " + file.name)
                    dialog.setMessage(R.string.dialog_text_sample_deletion_failure)
                    break
                }
            }
        }
        dialog.display(activity)
    }

    private fun deleteResults() {
        val ret = deleteFiles()
        val dialog: DialogInstance = if (ret) {
            DialogInstance(R.string.dialog_title_file_deletion, R.string.dialog_text_file_deletion_success)
        } else {
            DialogInstance(R.string.dialog_title_file_deletion, R.string.dialog_text_file_deletion_failure)
        }
        dialog.display(activity)
    }

    override fun onPreferenceTreeClick(preference: Preference): Boolean {
        val dialog: AlertDialog.Builder = AlertDialog.Builder(requireActivity())
                .setTitle(R.string.dialog_title_warning)
                .setMessage(R.string.dialog_text_deletion_confirmation)
                .setNeutralButton(R.string.dialog_btn_cancel, null)
        when (preference.key) {
            "delete_saves_key" -> {
                dialog.setNegativeButton(R.string.dialog_btn_continue) { _: DialogInterface?, _: Int -> deleteResults() }
                dialog.show()
            }
            "connect_key" -> {
                if (googleConnectionHandler.isGooglePlayServicesAvailable(requireActivity(), true)) {
                    googleConnectionHandler.signIn()
                }
            }
            "disconnect_key" -> {
                googleConnectionHandler.signOut()
                updateGoogleButton()
            }
            "delete_samples_key" -> {
                dialog.setNegativeButton(R.string.dialog_btn_continue) { _: DialogInterface?, _: Int -> deleteSamples() }
                dialog.show()
            }
            "about_key" -> {
                val intent = Intent(activity, AboutActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
                startActivity(intent)
            }
            "storage_key" -> {
            }
            "screenshot_upload_key" -> {
            }
            "screenshot_upload_all_key" -> {
            }
            else -> Log.e(TAG, "Unknown preference selected: " + preference.key)
        }
        return super.onPreferenceTreeClick(preference)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == Constants.RequestCodes.GOOGLE_CONNECTION) {
            googleConnectionHandler.setGoogleSignInClient(requireContext(), requireActivity())
            val errStr = googleConnectionHandler.handleSignInResult(data)
            if (errStr == null) {
                updateGoogleButton()
            } else {
                val dialogInstance = DialogInstance(R.string.dialog_title_error, R.string.dialog_text_err_google)
                dialogInstance.display(context)
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun updateGoogleButton() {
        if (!googleConnectionHandler.isConnected) {
            if (findPreference<Preference?>("connect_key") == null) {
                val preference = findPreference<Preference>("disconnect_key")
                preference?.title = getString(R.string.connect_pref)
                preference?.key = "connect_key"
            }
        } else {
            if (findPreference<Preference?>("disconnect_key") == null) {
                val preference = findPreference<Preference>("connect_key")
                preference?.title = getString(R.string.disconnect_pref) + " " + googleConnectionHandler.account?.email
                preference?.key = "disconnect_key"
            }
        }
    }

    override fun onPause() {
        googleConnectionHandler.unsetGoogleSignInClient()
        requireActivity().unregisterReceiver(br)
        onScrollListener?.let { listView.removeOnScrollListener(it)}
        onScrollListener = null
        super.onPause()
    }

    companion object {
        private val TAG = SettingsFragment::class.java.name
    }
}