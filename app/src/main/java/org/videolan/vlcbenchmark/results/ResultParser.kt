/*
 *****************************************************************************
 * ResultParser.kt
 *****************************************************************************
 * Copyright © 2020-2021 VLC authors and VideoLAN
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

package org.videolan.vlcbenchmark.results

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.util.Log
import androidx.preference.PreferenceManager
import org.videolan.vlcbenchmark.Constants
import org.videolan.vlcbenchmark.Constants.ResultCodes
import org.videolan.vlcbenchmark.R
import org.videolan.vlcbenchmark.VLCWorkerModel
import org.videolan.vlcbenchmark.results.types.ResultPlayback
import org.videolan.vlcbenchmark.results.types.ResultQuality
import org.videolan.vlcbenchmark.results.types.ResultSpeed
import org.videolan.vlcbenchmark.tests.Test
import org.videolan.vlcbenchmark.tests.TestController
import org.videolan.vlcbenchmark.tests.types.TestQuality
import java.io.BufferedReader
import java.io.InputStreamReader

class ResultParser {

     private fun getStacktrace(context: Context, uri: Uri): String {
          var stacktrace = ""
          try {
               val fin = context.contentResolver.openInputStream(uri)
               if (fin == null) {
                    Log.e(TAG, "getStacktrace: failed to open stack trace input stream")
                    return ""
               }
               val streamReader = InputStreamReader(fin)
               val bf = BufferedReader(streamReader)
               val stacktraceContent = StringBuilder()
               var line = bf.readLine()
               while (line != null) {
                    stacktraceContent.append(line)
                    stacktraceContent.append("\n")
                    line = bf.readLine()
               }
               stacktrace = stacktraceContent.toString()
               fin.close()
               streamReader.close()
               bf.close()
          } catch (e: Exception) {
               e.printStackTrace()
          }
          return stacktrace
     }

     private fun getResultInstance(test: Test) : ResultModel? {
          return when (test.type) {
               Constants.TestType.QUALITY -> ResultQuality(test.sample.name, test.hardware)
               Constants.TestType.PLAYBACK -> ResultPlayback(test.sample.name, test.hardware)
               Constants.TestType.SPEED -> ResultSpeed(test.sample.name, test.hardware)
               Constants.TestType.UNKNOWN -> null
          }
     }

     private fun getCrashMessage(data: Intent?, context: Context) : String {
          return if (data != null && data.hasExtra("Error")) {
               data.getStringExtra("Error") ?: "Failed to get crash cause"
          } else if (data != null) {
               context.getString(R.string.result_vlc_crash)
          } else {
               try {
                    val packageContext: Context = context.createPackageContext(context.getString(R.string.vlc_package_name), 0)
                    val preferences = packageContext.getSharedPreferences(VLCWorkerModel.SHARED_PREFERENCE, Context.MODE_PRIVATE)
                    preferences.getString(VLCWorkerModel.SHARED_PREFERENCE_STACK_TRACE, "") ?: "Failed to get crash cause"
               } catch (e: PackageManager.NameNotFoundException) {
                    e.message;"Failed to get crash cause"
               }
          }
     }

     fun parse(context: Context, data: Intent?, resultCode: Int, testController: TestController) : ResultModel? {
          val errorMessage: String
          var stacktrace = ""
          val result = getResultInstance(testController.getCurrentTest()) ?: return null
          if (resultCode != ResultCodes.RESULT_OK) {
               errorMessage = when (resultCode) {
                    ResultCodes.RESULT_CANCELED -> context.getString(R.string.result_canceled)
                    ResultCodes.RESULT_NO_HW -> context.getString(R.string.result_no_hw)
                    ResultCodes.RESULT_CONNECTION_FAILED -> context.getString(R.string.result_connection_failed)
                    ResultCodes.RESULT_PLAYBACK_ERROR -> context.getString(R.string.result_playback_error)
                    ResultCodes.RESULT_HARDWARE_ACCELERATION_ERROR -> context.getString(R.string.result_hardware_acceleration_error)
                    ResultCodes.RESULT_VIDEO_TRACK_LOST -> context.getString(R.string.result_video_track_lost)
                    ResultCodes.RESULT_VLC_CRASH -> getCrashMessage(data, context)
                    else -> context.getString(R.string.result_unknown)
               }

               val sharedPref = PreferenceManager.getDefaultSharedPreferences((context as Activity).applicationContext)
               val getStacktrace = sharedPref.getBoolean(context.getString(R.string.vlc_failure_stacktrace_key), false)
               if (resultCode == ResultCodes.RESULT_VLC_CRASH && getStacktrace) {
                    val uri = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                         data?.getParcelableExtra("stacktrace_uri", Uri::class.java)
                    } else {
                         data?.getParcelableExtra<Uri>("stacktrace_uri")
                    }
                    uri?.let {
                         stacktrace = getStacktrace(context, it)
                    }
               } else if (resultCode == ResultCodes.RESULT_VLC_CRASH && !getStacktrace)
                    stacktrace = context.getString(R.string.result_no_stacktrack_perm)

               result.setFailure(errorMessage, stacktrace)
          } else {
               when (result) {
                    is ResultPlayback -> {
                         data?.let{
                              result.setResults(
                                   data.getIntExtra("number_of_dropped_frames", 0),
                                   data.getIntExtra("late_frames", 0),
                                   data.getIntExtra("displayed_frames", 0),
                              )
                         }
                    }
                    is ResultQuality -> {
                         result.setResults(context, testController.getCurrentTest() as TestQuality)
                    }
                    is ResultSpeed -> {
                         data?.let { result.setResults(data.getFloatExtra("speed", 0.0f).toDouble()) }
                    }
               }
          }
          return result
     }

     companion object {
          @Suppress("UNUSED")
          private val TAG = this::class.java.name
     }
}