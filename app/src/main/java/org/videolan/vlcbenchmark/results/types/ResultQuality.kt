/*
 *****************************************************************************
 * ResultQuality.kt
 *****************************************************************************
 * Copyright © 2020-2021 VLC authors and VideoLAN
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

package org.videolan.vlcbenchmark.results.types

import android.app.Activity
import android.content.Context
import android.util.Log
import androidx.preference.PreferenceManager
import kotlinx.android.parcel.Parcelize
import org.json.JSONArray
import org.json.JSONObject
import org.videolan.vlcbenchmark.Constants
import org.videolan.vlcbenchmark.R
import org.videolan.vlcbenchmark.results.ResultModel
import org.videolan.vlcbenchmark.tests.types.TestQuality
import org.videolan.vlcbenchmark.benchmark.ScreenshotValidator
import org.videolan.vlcbenchmark.tools.StorageManager
import org.videolan.vlcbenchmark.tools.StorageManager.getInternalDirStr
import java.io.File

@Parcelize
class ResultQuality(
        override var name: String = "",
        override var hardware: Boolean = false,
        override var score: Double = 20.0,
        override var maxScore: Double = 20.0,
        override var type: Constants.ResultType = Constants.ResultType.QUALITY,
        override var crash: String = "",
        override var stacktrace: String = "",
        var percentOfBadScreenshot: Double = 0.0,
        var screenshotNames: ArrayList<String> = ArrayList(),
        var screenshotScores: ArrayList<Int> = ArrayList()
) : ResultModel(name, hardware, score, maxScore, type, crash, stacktrace) {

    constructor(jsonObject: JSONObject) : this() {
        this.name = jsonObject.getString("name")
        this.score = jsonObject.getInt("score").toDouble()
        this.hardware = jsonObject.getBoolean("hardware")
        this.crash = jsonObject.getString("crash")
        this.stacktrace = jsonObject.getString("stacktrace")
        this.percentOfBadScreenshot = jsonObject.getInt("percent_of_bad_screenshots").toDouble()
        val screenshots = jsonObject.getJSONArray("screenshots")
        for (i in 0 until screenshots.length()) {
            this.screenshotNames.add(screenshots[i].toString())
        }
        val screenshotsScores = jsonObject.getJSONArray("screenshot_scores")
        for (i in 0 until screenshotsScores.length()) {
            this.screenshotScores.add(screenshotsScores[i].toString().toInt())
        }
    }

    override fun jsonDump(): JSONObject {
        val jsonObject = super.jsonDump()
        jsonObject.put("percent_of_bad_screenshots", percentOfBadScreenshot.toInt())
        jsonObject.put("screenshots", JSONArray() )
        val screenshotScoresJSONArray = JSONArray(screenshotScores)
        jsonObject.put("screenshot_scores", screenshotScoresJSONArray)
        jsonObject.put("max_score", this.maxScore)
        return jsonObject
    }

    fun jsonDumpWithScreenshots(): JSONObject {
        val jsonObject = jsonDump()
        jsonObject.put("percent_of_bad_screenshots", percentOfBadScreenshot.toInt())
        val screenshotJSONArray = JSONArray(screenshotNames)
        jsonObject.put("screenshots", screenshotJSONArray)
        val screenshotScoresJSONArray = JSONArray(screenshotScores)
        jsonObject.put("screenshot_scores", screenshotScoresJSONArray)
        return jsonObject
    }

    fun setResults(context: Context, test: TestQuality) {
        val screenshotDir = getInternalDirStr(StorageManager.screenshotFolder)
        val numberOfScreenshot: Int = test.timestamps.size
        val colors = test.colors
        val sharedPref = PreferenceManager.getDefaultSharedPreferences((context as Activity).applicationContext)
        val screenUploadAll = sharedPref.getBoolean(context.getString(R.string.screenshot_upload_all_key), false)

        var badScreenshots = 0
        for (i in 0 until numberOfScreenshot) {
            val filePath = "$screenshotDir/$SCREENSHOT_NAMING$i.png"
            val file = File(filePath)
            val res = ScreenshotValidator.validateScreenshot(filePath, colors[i])
            res.first?.let {
                if (!it) {
                    badScreenshots++
                }
                if (!it || screenUploadAll) {
                    var fileName: String = test.sample.name.split("_")[0] + "_" + test.getTypeString()
                    fileName += "_$i.png"
                    val renamedFile = File("$screenshotDir/$fileName")
                    if (!file.renameTo(renamedFile)) {
                        fileName = ""
                    }
                    this.screenshotNames.add(fileName)
                    val screenshotScore = res.second
                    if (screenshotScore != null) {
                        this.screenshotScores.add(screenshotScore)
                    } else {
                        Log.e(TAG, "setResults: screenshot score is null")
                    }
                } else {
                    Log.d(TAG, "setResults: not uploading everything")
                }
            }
            file.delete()
        }
        this.percentOfBadScreenshot = badScreenshots.toDouble() / numberOfScreenshot * 100
        this.score -= badScreenshots.toDouble() / numberOfScreenshot * maxScore
    }

    companion object {
        @Suppress("UNUSED")
        private val TAG = this::class.java.name
        private const val SCREENSHOT_NAMING = "Screenshot_"
    }
}