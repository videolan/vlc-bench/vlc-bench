package org.videolan.vlcbenchmark.results

import android.animation.Animator
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.Transformation
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
//import kotlinx.android.synthetic.main.result_sample_row.view.result_layout
//import kotlinx.android.synthetic.main.result_sample_row.view.*
import org.videolan.vlcbenchmark.Constants
import org.videolan.vlcbenchmark.R
import org.videolan.vlcbenchmark.databinding.ResultSampleRowBinding
import org.videolan.vlcbenchmark.results.types.ResultPlayback
import org.videolan.vlcbenchmark.results.types.ResultQuality
import org.videolan.vlcbenchmark.results.types.ResultSpeed
import org.videolan.vlcbenchmark.tools.FormatStr
import org.videolan.vlcbenchmark.tools.Util

class ResultSampleAdapter(val results: ResultSampleList, val openedList: ArrayList<Int>) : RecyclerView.Adapter<ResultSampleAdapter.ViewHolder>() {
    
    companion object {
        @Suppress("UNUSED")
        private val TAG = this::class.java.name
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ResultSampleRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var isExpanded = openedList.contains(position)
        holder.itemView.isActivated = isExpanded
        if (isExpanded) {
            holder.itemView.findViewById<ConstraintLayout>(R.id.result_layout).setBackgroundColor(ContextCompat.getColor(holder.itemView.context, R.color.grey100))
            holder.itemView.findViewById<ImageView>(R.id.result_handle).rotation = 90f
            holder.itemView.findViewById<ConstraintLayout>(R.id.result_collapsible).visibility = View.VISIBLE
        } else {
            holder.itemBinding.resultLayout.setBackgroundColor(ContextCompat.getColor(holder.itemView.context, R.color.pureWhite))
            holder.itemBinding.resultHandle.rotation = 0f
            holder.itemBinding.resultCollapsible.visibility = View.GONE
        }
        holder.itemBinding.root.setOnFocusChangeListener { v, hasFocus ->
            if (Util.isAndroidTV(v.context)) {
                if (hasFocus) {
                    holder.itemBinding.resultLayout.setBackgroundColor(ContextCompat.getColor(holder.itemView.context, R.color.grey200))
                } else {
                    if (isExpanded)
                        holder.itemBinding.resultLayout.setBackgroundColor(ContextCompat.getColor(holder.itemView.context, R.color.grey100))
                    else
                        holder.itemBinding.resultLayout.setBackgroundColor(ContextCompat.getColor(holder.itemView.context, R.color.pureWhite))
                }
            }
        }
        val clickView = if (Util.isAndroidTV(holder.itemView.context))
            holder.itemBinding.root
        else
            holder.itemBinding.resultLayout
        clickView.setOnClickListener {
            if (isExpanded) {
                openedList.remove(position)
                collapse(holder)
                isExpanded = false
            } else {
                openedList.add(position)
                expand(holder)
                isExpanded = true
            }
        }
        holder.setDataResults(results.results[position])
    }

    private fun expand(holder: ViewHolder) {
        val view = holder.itemBinding.resultCollapsible
        val handle = holder.itemBinding.resultHandle
        handle.animate().rotation(90f).setListener(null)
        view.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        val measuredHeight = view.measuredHeight
        view.layoutParams.height = 0
        view.visibility = View.VISIBLE
        val animation: Animation = object : Animation() {
            override fun applyTransformation(interpolatedTime: Float, t: Transformation?) {
                view.layoutParams.height =
                    if (interpolatedTime == 1f) ViewGroup.LayoutParams.WRAP_CONTENT
                    else (measuredHeight * interpolatedTime).toInt()
                view.requestLayout()
            }
        }
        animation.duration = 300
        holder.itemBinding.resultLayout.setBackgroundColor(ContextCompat.getColor(view.context, R.color.grey100))
        view.startAnimation(animation)
    }

    private fun collapse(holder: ViewHolder) {
        val view = holder.itemBinding.resultCollapsible
        val handle = holder.itemBinding.resultHandle
        handle.animate().rotation(0f).setListener(object : Animator.AnimatorListener {
            override fun onAnimationEnd(animation: Animator) {
                holder.itemBinding.resultLayout.setBackgroundColor(ContextCompat.getColor(view.context, R.color.pureWhite))
            }
            override fun onAnimationRepeat(animation: Animator) {}
            override fun onAnimationCancel(animation: Animator) {}
            override fun onAnimationStart(animation: Animator) {}
        })
        val measuredHeight = view.measuredHeight
        val animation: Animation = object : Animation() {
            override fun applyTransformation(interpolatedTime: Float, t: Transformation?) {
                if (interpolatedTime == 1f) {
                    view.visibility = View.GONE
                } else {
                    view.layoutParams.height =
                        measuredHeight - (measuredHeight * interpolatedTime).toInt()
                    view.requestLayout()
                }
            }
        }
        animation.duration = 300
        view.startAnimation(animation)
    }

    override fun getItemCount(): Int {
        return results.results.size
    }

    inner class ViewHolder(val itemBinding: ResultSampleRowBinding) : RecyclerView.ViewHolder(itemBinding.root) {
        lateinit var result: ResultSample
        private var hardwareScore: Double = 0.0
        private var softwareScore: Double = 0.0
        private var hardwareMaxScore: Double = 0.0
        private var softwareMaxScore: Double = 0.0

        private val TAG = this::class.java.name

        private fun setSoftwareTitleVisible() {
            itemBinding.softwareTitle.visibility = View.VISIBLE
            itemBinding.softwareScore.visibility = View.VISIBLE
        }

        private fun setHardwareTitleVisible() {
            itemBinding.hardwareTitle.visibility = View.VISIBLE
            itemBinding.hardwareScore.visibility = View.VISIBLE
        }

        private fun resetVisibility() {
            itemBinding.softwareTitle.visibility = View.GONE
            itemBinding.softwareScore.visibility = View.GONE

            itemBinding.softwareTitlePlayback.visibility = View.GONE
            itemBinding.softwarePlaybackScore.visibility = View.GONE
            itemBinding.softwarePlaybackFramesDropped.visibility = View.GONE
            itemBinding.softwarePlaybackFramesDroppedScore.visibility = View.GONE
            itemBinding.softwarePlaybackWarnings.visibility = View.GONE
            itemBinding.softwarePlaybackWarningsScore.visibility = View.GONE
            itemBinding.softwarePlaybackCrash.visibility = View.GONE

            itemBinding.softwareTitleQuality.visibility = View.GONE
            itemBinding.softwareQualityScore.visibility = View.GONE
            itemBinding.softwareQualityBadScreenshots.visibility = View.GONE
            itemBinding.softwareQualityBadScreenshotsScore.visibility = View.GONE
            itemBinding.softwareQualityCrash.visibility = View.GONE

            itemBinding.softwareTitleSpeed.visibility = View.GONE
            itemBinding.softwareSpeedScore.visibility = View.GONE
            itemBinding.softwareSpeedSpeed.visibility = View.GONE
            itemBinding.softwareSpeedSpeedScore.visibility = View.GONE
            itemBinding.softwareSpeedCrash.visibility = View.GONE

            itemBinding.hardwareTitle.visibility = View.GONE
            itemBinding.hardwareScore.visibility = View.GONE

            itemBinding.hardwareTitlePlayback.visibility = View.GONE
            itemBinding.hardwarePlaybackScore.visibility = View.GONE
            itemBinding.hardwarePlaybackFramesDropped.visibility = View.GONE
            itemBinding.hardwarePlaybackFramesDroppedScore.visibility = View.GONE
            itemBinding.hardwarePlaybackWarnings.visibility = View.GONE
            itemBinding.hardwarePlaybackWarningsScore.visibility = View.GONE
            itemBinding.hardwarePlaybackCrash.visibility = View.GONE

            itemBinding.hardwareTitleQuality.visibility = View.GONE
            itemBinding.hardwareQualityScore.visibility = View.GONE
            itemBinding.hardwareQualityBadScreenshots.visibility = View.GONE
            itemBinding.hardwareQualityBadScreenshotsScore.visibility = View.GONE
            itemBinding.hardwareQualityCrash.visibility = View.GONE

            itemBinding.hardwareTitleSpeed.visibility = View.GONE
            itemBinding.hardwareSpeedScore.visibility = View.GONE
            itemBinding.hardwareSpeedSpeed.visibility = View.GONE
            itemBinding.hardwareSpeedSpeedScore.visibility = View.GONE
            itemBinding.hardwareSpeedCrash.visibility = View.GONE
        }

        fun setDataResults(res: ResultSample) {
            result = res

            hardwareScore = 0.0
            softwareScore = 0.0
            hardwareMaxScore = 0.0
            softwareMaxScore = 0.0
            resetVisibility()

            for (r in result.results) {
                when {
                    !r.hardware && r.type == Constants.ResultType.PLAYBACK -> {
                        setSoftwareTitleVisible()

                        itemBinding.softwareTitlePlayback.visibility = View.VISIBLE
                        itemBinding.softwarePlaybackScore.visibility = View.VISIBLE
                        itemBinding.softwarePlaybackFramesDropped.visibility = View.VISIBLE
                        itemBinding.softwarePlaybackFramesDroppedScore.visibility = View.VISIBLE
                        itemBinding.softwarePlaybackPercentageFramesDropped.visibility = View.VISIBLE
                        itemBinding.softwarePlaybackPercentageFramesDroppedScore.visibility = View.VISIBLE
                        itemBinding.softwarePlaybackWarnings.visibility = View.VISIBLE
                        itemBinding.softwarePlaybackWarningsScore.visibility = View.VISIBLE

                        val frames = (r as ResultPlayback).framesDropped
                        val percentage = if (r.totalFrames != 0)
                            FormatStr.format2Dec(frames.toDouble() / r.totalFrames.toDouble() * 100.0)
                        else "0"
                        itemBinding.softwarePlaybackFramesDroppedScore.text =
                            String.format(itemView.context.getString(R.string.score), frames, r.totalFrames)
                        itemBinding.softwarePlaybackPercentageFramesDroppedScore.text = String.format(itemView.context.getString(R.string.decimal_percent), percentage)

                        itemBinding.softwarePlaybackWarningsScore.text =
                            String.format(itemView.context.getString(R.string.simple_score), r.warnings)
                        if (r.crash != "") {
                            itemBinding.softwarePlaybackCrash.visibility = View.VISIBLE
                            itemBinding.softwarePlaybackCrash.text =
                                String.format(itemView.context.getString(R.string.crash), r.crash)
                        }
                        softwareScore += r.score
                        softwareMaxScore += r.maxScore
                        itemBinding.softwarePlaybackScore.text =
                            String.format(itemView.context.getString(R.string.score), r.score.toInt(), r.maxScore.toInt())
                    }
                    !r.hardware && r.type == Constants.ResultType.QUALITY -> {
                        setSoftwareTitleVisible()

                        itemBinding.softwareTitleQuality.visibility = View.VISIBLE
                        itemBinding.softwareQualityScore.visibility = View.VISIBLE
                        itemBinding.softwareQualityBadScreenshots.visibility = View.VISIBLE
                        itemBinding.softwareQualityBadScreenshotsScore.visibility = View.VISIBLE

                        val screenshots = (r as ResultQuality).percentOfBadScreenshot
                        itemBinding.softwareQualityBadScreenshotsScore.text =
                            String.format(itemView.context.getString(R.string.simple_percent), screenshots.toInt())
                        if (r.crash != "") {
                            itemBinding.softwareQualityCrash.visibility = View.VISIBLE
                            itemBinding.softwareQualityCrash.text =
                                String.format(itemView.context.getString(R.string.crash), r.crash)
                        }
                        softwareScore += r.score
                        softwareMaxScore += r.maxScore
                        itemBinding.softwareQualityScore.text =
                            String.format(itemView.context.getString(R.string.score), r.score.toInt(), r.maxScore.toInt())
                    }
                    !r.hardware && r.type == Constants.ResultType.SPEED -> {
                        setSoftwareTitleVisible()

                        itemBinding.softwareTitleSpeed.visibility = View.VISIBLE
                        itemBinding.softwareSpeedScore.visibility = View.VISIBLE
                        itemBinding.softwareSpeedSpeed.visibility = View.VISIBLE
                        itemBinding.softwareSpeedSpeedScore.visibility = View.VISIBLE

                        val speed = (r as ResultSpeed).speed
                        itemBinding.softwareSpeedSpeedScore.text =
                            String.format(itemView.context.getString(R.string.simple_decimal_str), FormatStr.format2Dec(speed))
                        if (r.crash != "") {
                            itemBinding.softwareSpeedCrash.visibility = View.VISIBLE
                            itemBinding.softwareSpeedCrash.text =
                                String.format(itemView.context.getString(R.string.crash), r.crash)
                        }
                        softwareScore += r.score
                        softwareMaxScore += r.maxScore
                        itemBinding.softwareSpeedScore.text =
                            String.format(itemView.context.getString(R.string.score), r.score.toInt(), r.maxScore.toInt())
                    }
                    r.hardware && r.type == Constants.ResultType.PLAYBACK -> {
                        setHardwareTitleVisible()

                        itemBinding.hardwareTitlePlayback.visibility = View.VISIBLE
                        itemBinding.hardwarePlaybackScore.visibility = View.VISIBLE
                        itemBinding.hardwarePlaybackFramesDropped.visibility = View.VISIBLE
                        itemBinding.hardwarePlaybackFramesDroppedScore.visibility = View.VISIBLE
                        itemBinding.hardwarePlaybackPercentageFramesDropped.visibility = View.VISIBLE
                        itemBinding.hardwarePlaybackPercentageFramesDroppedScore.visibility = View.VISIBLE
                        itemBinding.hardwarePlaybackWarnings.visibility = View.VISIBLE
                        itemBinding.hardwarePlaybackWarningsScore.visibility = View.VISIBLE

                        val frames = (r as ResultPlayback).framesDropped
                        val percentage = if (r.totalFrames != 0)
                            FormatStr.format2Dec(frames.toDouble() / r.totalFrames.toDouble() * 100.0)
                        else "0"
                        itemBinding.hardwarePlaybackFramesDroppedScore.text =
                            String.format(itemView.context.getString(R.string.score), frames, r.totalFrames)
                        itemBinding.hardwarePlaybackPercentageFramesDroppedScore.text =
                            String.format(itemView.context.getString(R.string.decimal_percent), percentage)
                        itemBinding.hardwarePlaybackWarningsScore.text =
                            String.format(itemView.context.getString(R.string.simple_score), r.warnings)
                        if (r.crash != "") {
                            itemBinding.hardwarePlaybackCrash.visibility = View.VISIBLE
                            itemBinding.hardwarePlaybackCrash.text =
                                String.format(itemView.context.getString(R.string.crash), r.crash)
                        }
                        hardwareScore += r.score
                        hardwareMaxScore += r.maxScore
                        itemBinding.hardwarePlaybackScore.text =
                            String.format(itemView.context.getString(R.string.score), r.score.toInt(), r.maxScore.toInt())
                    }
                    r.hardware && r.type == Constants.ResultType.QUALITY -> {
                        setHardwareTitleVisible()

                        itemBinding.hardwareTitleQuality.visibility = View.VISIBLE
                        itemBinding.hardwareQualityScore.visibility = View.VISIBLE
                        itemBinding.hardwareQualityBadScreenshots.visibility = View.VISIBLE
                        itemBinding.hardwareQualityBadScreenshotsScore.visibility = View.VISIBLE

                        val screenshots = (r as ResultQuality).percentOfBadScreenshot
                        itemBinding.hardwareQualityBadScreenshotsScore.text =
                            String.format(itemView.context.getString(R.string.simple_percent), screenshots.toInt())
                        if (r.crash != "") {
                            itemBinding.hardwareQualityCrash.visibility = View.VISIBLE
                            itemBinding.hardwareQualityCrash.text =
                                String.format(itemView.context.getString(R.string.crash), r.crash)
                        }
                        hardwareScore += r.score
                        hardwareMaxScore += r.maxScore
                        itemBinding.hardwareQualityScore.text =
                            String.format(itemView.context.getString(R.string.score), r.score.toInt(), r.maxScore.toInt())
                    }
                    r.hardware && r.type == Constants.ResultType.SPEED -> {
                        setHardwareTitleVisible()

                        itemBinding.hardwareTitleSpeed.visibility = View.VISIBLE
                        itemBinding.hardwareSpeedScore.visibility = View.VISIBLE
                        itemBinding.hardwareSpeedSpeed.visibility = View.VISIBLE
                        itemBinding.hardwareSpeedSpeedScore.visibility = View.VISIBLE

                        val speed = (r as ResultSpeed).speed
                        itemBinding.hardwareSpeedSpeedScore.text =
                            String.format(itemView.context.getString(R.string.simple_decimal_str), FormatStr.format2Dec(speed))
                        if (r.crash != "") {
                            itemBinding.hardwareSpeedCrash.visibility = View.VISIBLE
                            itemBinding.hardwareSpeedCrash.text =
                                String.format(itemView.context.getString(R.string.crash), r.crash)
                        }
                        hardwareScore += r.score
                        hardwareMaxScore += r.maxScore
                        itemBinding.hardwareSpeedScore.text =
                            String.format(itemView.context.getString(R.string.score), r.score.toInt(), r.maxScore.toInt())
                    }
                }
            }
            itemBinding.softwareScore.text = String.format(itemView.context.getString(R.string.score), softwareScore.toInt(), softwareMaxScore.toInt())
            itemBinding.hardwareScore.text = String.format(itemView.context.getString(R.string.score), hardwareScore.toInt(), hardwareMaxScore.toInt())
            val percent = ((softwareScore + hardwareScore) / (softwareMaxScore + hardwareMaxScore) * 100.0).toInt()
            itemBinding.resultScore.text = String.format(itemView.context.getString(R.string.simple_percent), percent)
            itemBinding.resultSampleName.text = result.name
        }
    }
}