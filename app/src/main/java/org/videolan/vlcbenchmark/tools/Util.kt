/*
 *****************************************************************************
 * Util.kt
 *****************************************************************************
 * Copyright © 2017 - 2020 VLC authors and VideoLAN
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/
package org.videolan.vlcbenchmark.tools

import android.app.UiModeManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.IntentFilter
import android.content.res.Configuration
import android.net.ConnectivityManager
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import com.google.android.material.snackbar.Snackbar
import java.io.*
import java.util.concurrent.Executors

object Util {
    private val TAG = Util::class.java.name
    private var threadPool = Executors.newSingleThreadExecutor()
    private var handler = Handler(Looper.getMainLooper())

    fun errorSnackbar(view: View, errorStringId: Int) {
        Snackbar.make(view, errorStringId, Snackbar.LENGTH_LONG).show()
    }

    /**
     * Tool method to check if the device is currently connected to WIFI or LAN
     *
     * @return true if connected to WIFI or LAN else false
     */
    fun hasWifiAndLan(context: Context): Boolean {
        var networkEnabled = false
        val connectivity = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivity.activeNetworkInfo
        if (networkInfo != null && networkInfo.isConnected &&
                networkInfo.type != ConnectivityManager.TYPE_MOBILE) {
            networkEnabled = true
        }
        return networkEnabled
    }

    private fun close(closeable: Closeable?): Boolean {
        closeable?.let {
            try {
                closeable.close()
                return true
            } catch (e: IOException) {
                Log.e(TAG, "Failed to close: $e")
            }
        }
        return false
    }

    @JvmStatic
    fun isAndroidTV(context: Context): Boolean {
        val uiModeManager = context.getSystemService(Context.UI_MODE_SERVICE) as UiModeManager
        return uiModeManager.currentModeType == Configuration.UI_MODE_TYPE_TELEVISION
    }

    @JvmStatic
    fun runInUiThread(runnable: () -> Unit) {
        handler.post(runnable)
    }

    @JvmStatic
    fun runInBackground(runnable: () -> Unit) {
        threadPool.execute(runnable)
    }

    @JvmStatic
    fun runInUiThread(runnable: Runnable) {
        handler.post(runnable)
    }

    @JvmStatic
    fun runInBackground(runnable: Runnable) {
        threadPool.execute(runnable)
    }

    fun Context.registerReceiverCompat(receiver: BroadcastReceiver, filter: IntentFilter, exported: Boolean) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU)
            registerReceiver(receiver, filter, if (exported) Context.RECEIVER_EXPORTED else Context.RECEIVER_NOT_EXPORTED)
        else
            registerReceiver(receiver, filter)
    }
}