/*
 *****************************************************************************
 * OnboardingActivity.kt
 *****************************************************************************
 * Copyright © 2019-2021 VLC authors and VideoLAN
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

package org.videolan.vlcbenchmark.onboarding

import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import org.videolan.vlcbenchmark.MainPage
import org.videolan.vlcbenchmark.R
import org.videolan.vlcbenchmark.databinding.ActivityOnboardingBinding

class OnboardingActivity : AppCompatActivity(), ViewPager.OnPageChangeListener {

    companion object {
        @Suppress("UNUSED")
        private val TAG = this::class.java.name

        private const val SHARED_PREFERENCE_ONBOARDING = "org.videolan.vlc.gui.video.benchmark.ONBOARDING"
        private const val PERMISSION_REQUEST = 1
    }
    private var permissions: ArrayList<String> = ArrayList()
    private lateinit var positionIndicators: Array<View>
    private lateinit var pager: ViewPager
    private lateinit var binding: ActivityOnboardingBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityOnboardingBinding.inflate(layoutInflater)

        // Checking if the onboarding has already been done
        val sharedPref = this.getPreferences(Context.MODE_PRIVATE)
        val onboardingDone = sharedPref.getBoolean(SHARED_PREFERENCE_ONBOARDING, false)

        if (onboardingDone)
            startMainPage()

        setContentView(binding.root)

        positionIndicators = arrayOf(
                binding.onboardingIndicator0,
                binding.onboardingIndicator1,
                binding.onboardingIndicator2,
        )

        pager = binding.onboarddingPager
        val layouts = arrayOf(
                R.layout.layout_onboarding_intro,
                R.layout.layout_onboarding_download,
                R.layout.layout_onboarding_warnings
        )
        pager.adapter = OnboardingPagerAdapter(this, layouts)
        pager.currentItem = 0
        updateIndicator(0)
        updateButtons(0)
        pager.addOnPageChangeListener(this)

        // Check if Android TV / chromebook for pad / keyboard inputs -> manually handle focus
        if (packageManager.hasSystemFeature("android.software.leanback")
                or packageManager.hasSystemFeature("org.chromium.arc.device_management")) {
            setFocusListeners()
            binding.onboardingBtnNext.requestFocus()
            // have to call onFocusChange manually here, requestFocus doesn't seem to call the callback
            onFocusChange(binding.onboardingBtnNext, true)
        }
    }

    // In case of android tv, change the background color to clearly indicate focus
    private fun onFocusChange(view: View, hasFocus: Boolean) : Unit {
        if (hasFocus) {
            view.setBackgroundColor(resources.getColor(R.color.grey400transparent))
        } else {
            view.setBackgroundColor(resources.getColor(R.color.white))
        }
    }

    // Setting callbacks to change background on focus change
    private fun setFocusListeners() {
        binding.onboardingBtnNext.setOnFocusChangeListener(::onFocusChange)
        binding.onboardingBtnPrevious.setOnFocusChangeListener(::onFocusChange)

        // in case of a tactile swipe, and then an keyboard / pad input, the viewpager
        // gets the focus. To stop that, focus is redirected to next button
        // or done button in the case of the last onboarding view
        pager.setOnFocusChangeListener { _, _ ->
            if (pager.currentItem == 2) {
                binding.onboardingBtnDone.requestFocus()
            } else {
                binding.onboardingBtnNext.requestFocus()
            }
        }
    }

    private fun updateIndicator(position: Int) {
        for (i in positionIndicators.indices) {
            var layoutParams: LinearLayout.LayoutParams
            if (i == position) {
                layoutParams = LinearLayout.LayoutParams(
                        resources.getDimension(R.dimen.selected_indicator).toInt(),
                        resources.getDimension(R.dimen.selected_indicator).toInt())
            } else {
                layoutParams = LinearLayout.LayoutParams(
                        resources.getDimension(R.dimen.unselected_indicator).toInt(),
                        resources.getDimension(R.dimen.unselected_indicator).toInt())
            }
            layoutParams.setMargins(
                    resources.getDimension(R.dimen.indicator_margin).toInt(),
                    resources.getDimension(R.dimen.indicator_margin).toInt(),
                    resources.getDimension(R.dimen.indicator_margin).toInt(),
                    resources.getDimension(R.dimen.indicator_margin).toInt())
            positionIndicators[i].layoutParams = layoutParams
        }
    }

    private fun updateButtons(position: Int) {
        val nextBtn = binding.onboardingBtnNext
        val doneBtn = binding.onboardingBtnDone
        val prevBtn = binding.onboardingBtnPrevious
        if (position == 2) {
            nextBtn.visibility = View.INVISIBLE
            doneBtn.visibility = View.VISIBLE
            doneBtn.requestFocus()
        } else {
            nextBtn.visibility = View.VISIBLE
            doneBtn.visibility = View.INVISIBLE
        }
        if (position == 0) {
            prevBtn.visibility = View.INVISIBLE
            nextBtn.requestFocus()
        } else {
            prevBtn.visibility = View.VISIBLE
        }
    }


    fun clickNextPage(view: View) {
        if (pager.currentItem != pager.childCount) {
            pager.currentItem += 1
        }
    }

    fun clickPreviousPage(view: View) {
        if (pager.currentItem != 0) {
            pager.currentItem -= 1
        }
    }

    fun clickDone(view: View) {
        startMainPage()
    }

    override fun onPageSelected(position: Int) {
        updateIndicator(position)
        updateButtons(position)
    }

    override fun onPageScrollStateChanged(state: Int) {}

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

    private fun startMainPage() {
        // Save the fact that the onboarding was done
        val sharedPref = this.getPreferences(Context.MODE_PRIVATE)
        val editor = sharedPref.edit()
        editor.putBoolean(SHARED_PREFERENCE_ONBOARDING, true)
        editor.apply()

        // Start main page
        val intent = Intent(this, MainPage::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }

    class OnboardingPagerAdapter(context: Context, layouts: Array<Int>) : PagerAdapter() {
        private val mLayouts: Array<Int> = layouts
        private val mContext : Context = context

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val inflater = LayoutInflater.from(mContext)
            val layout = inflater.inflate(mLayouts[position], container, false)
            container.addView(layout)
            return layout
        }

        override fun destroyItem(container: ViewGroup, position: Int, view: Any) {
            container.removeView(view as View)
        }

        override fun isViewFromObject(view: View, viewObject: Any): Boolean {
                return view == viewObject
        }

        override fun getCount(): Int {
            return mLayouts.size
        }
    }
}