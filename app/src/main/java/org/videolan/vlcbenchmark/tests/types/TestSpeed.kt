/*
 *****************************************************************************
 * TestSpeed.kt
 *****************************************************************************
 * Copyright © 2020-2021 VLC authors and VideoLAN
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

package org.videolan.vlcbenchmark.tests.types

import android.content.Context
import android.content.Intent
import kotlinx.android.parcel.Parcelize
import org.json.JSONObject
import org.videolan.vlcbenchmark.Constants
import org.videolan.vlcbenchmark.VLCWorkerModel
import org.videolan.vlcbenchmark.tests.Test
import org.videolan.vlcbenchmark.tests.TestSample
import org.videolan.vlcbenchmark.tools.StorageManager

@Parcelize
class TestSpeed(
        override var sample: TestSample = TestSample(),
        override var type: Constants.TestType = Constants.TestType.SPEED,
        override var hardware: Boolean = false
) : Test(sample, type, hardware){

    constructor(jsonObject: JSONObject) : this() {
        this.sample.name = jsonObject.getString("name")
        this.hardware = jsonObject.getBoolean("hardware")
        this.sample.url = jsonObject.getString("url")
        this.sample.checksum = jsonObject.getString("checksum")
        this.sample.size = jsonObject.getInt("size")
        this.sample.localUrl = StorageManager.getInternalDirStr(StorageManager.mediaFolder) + "/" + this.sample.name
    }

    override fun prepareIntent(context: Context, intent: Intent, listener: VLCWorkerModel.OnIntentCreatedListener) {
        intent.putExtra(Constants.Extras.ACTION, Constants.Extras.ACTION_SPEED)
        super.prepareIntent(context, intent, listener)
    }
}