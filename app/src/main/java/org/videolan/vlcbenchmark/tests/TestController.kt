/*
 *****************************************************************************
 * TestController.kt
 *****************************************************************************
 * Copyright © 2020-2021 VLC authors and VideoLAN
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

package org.videolan.vlcbenchmark.tests

import android.content.Context
import android.os.Parcelable
import android.util.Log
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue
import org.videolan.vlcbenchmark.R
import org.videolan.vlcbenchmark.tools.FormatStr

@Parcelize
class TestController(
        private var testList: @RawValue ArrayList<Test>,
        var version: String,
        var currentIndex: Int = 0
) : Parcelable {

    fun next() : Boolean {
        if (currentIndex + 1 < testList.size) {
            currentIndex += 1
            return true
        }
        return false
    }

    fun setCurrentTestIndex(index: Int) {
        this.currentIndex = index
    }

    fun dropTestList() {
        testList = ArrayList()
    }

    fun setTestList(testList: @RawValue ArrayList<Test>) {
        this.testList = testList
    }

    fun reset() {
        currentIndex = 0
    }

    fun getCurrentTest() : Test {
        return testList[currentIndex]
    }

    fun getTestNumber() : Int {
        return testList.size
    }

    fun getProgress(loopIndex: Int, loopTotal: Int) : Double {
        return (testList.size.toDouble() * loopIndex.toDouble() + currentIndex.toDouble()) /
                (testList.size.toDouble() * loopTotal.toDouble()) * 100.0
    }

    fun getProgressPercentString(context: Context, loopIndex: Int, loopTotal: Int ): String {
        return String.format(context.getString(R.string.progress_text_percent),
                FormatStr.format2Dec(getProgress(loopIndex, loopTotal)))
    }

    fun getProgressString(context: Context, loopIndex: Int, loopTotal: Int) : String {
        val prettyString = if (testList.size > 0) {
            testList[currentIndex].getPrettyTypeString()
        } else {
            ""
        }
        return if (loopTotal > 1) {
            String.format(
                    context.getString(R.string.progress_text_format_loop),
                    currentIndex + 1,
                    testList.size, prettyString,
                    loopIndex, loopTotal
            )
        } else {
            String.format(
                    context.getString(R.string.progress_text_format),
                    currentIndex + 1, testList.size, prettyString
            )
        }
    }

    fun getSampleList() : ArrayList<TestSample> {
        val sampleList = ArrayList<TestSample>()
        for (test in testList) {
            if (test.sample !in sampleList)
                sampleList.add(test.sample)
        }
        return sampleList
    }

}