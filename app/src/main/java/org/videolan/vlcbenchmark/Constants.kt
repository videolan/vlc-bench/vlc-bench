/*
 *****************************************************************************
 * Constants.kt
 *****************************************************************************
 * Copyright © 2017 - 2021 VLC authors and VideoLAN
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/
package org.videolan.vlcbenchmark

object Constants {
    const val ACTION_TRIGGER_SCREENSHOT = "org.videolan.vlcbenchmark.TRIGGER_SCREENSHOT"
    const val ACTION_CONTINUE_BENCHMARK = "org.videolan.vlc.gui.video.benchmark.CONTINUE_BENCHMARK"
    const val ACTION_START_DOWNLOAD = "org.videolan.vlcbenchmark.START_DOWNLOAD"
    const val ACTION_RESET_RUNNING = "org.videolan.vlcbenchmark.RESET_RUNNING"
    const val ACTION_CANCEL_DOWNLOAD = "org.videolan.vlcbenchmark.CANCEL_DOWNLOAD"
    const val ACTION_UPDATE_PROGRESS = "org.videolan.vlcbenchmark.UPDATE_PROGRESS"
    const val ACTION_DOWNLOAD_FINISHED = "org.videolan.vlcbenchmark.DOWNLOAD_FINISHED"
    const val ACTION_SERVICE_ERROR = "org.videolan.vlcbenchmark.SERVICE_ERROR"
    const val ACTION_STOP_BENCHMARK_SERVICE = "org.videolan.vlcbenchmark.STOP_BENCHMARK_SERVICE"
    const val ACTION_STOP_TRANSFERT_SERVICE = "org.videolan.vlcbenchmark.STOP_TRANSFERT_SERVICE"
    const val ACTION_CANCEL_SAMPLE_TRANSFERT = "org.videolan.vlcbenchmark.CANCEL_FILE_TRANSFERT"
    const val ACTION_START_SAMPLE_TRANSFER = "org.videolan.vlcbenchmark.START_SAMPLE_TRANSFER"
    const val ACTION_FINISHED_SAMPLE_TRANSFER = "org.videolan.vlcbenchmark.FINISHED_SAMPLE_TRANSFER"
    const val EXTRA_SAMPLE_TRANSFER_OLD_DIR = "org.videolan.vlcbenchmark.EXTRA_SAMPLE_TRANSFER_OLD_DIR"
    const val EXTRA_SAMPLE_TRANSFER_NEW_DIR = "org.videolan.vlcbenchmark.EXTRA_SAMPLE_TRANSFER_NEW_DIR"
    const val EXTRA_SERVICE_ERROR = "org.videolan.vlcbenchmark.EXTRA_SERVICE_ERROR"
    const val EXTRA_DOWNLOAD_PERCENT = "org.videolan.vlcbenchmark.EXTRA_DOWNLOAD_PERCENT"
    const val EXTRA_DOWNLOAD_STRING = "org.videolan.vlcbenchmark.EXTRA_DOWNLOAD_STRING"
    const val EXTRA_NOTIFICATION_ID = "org.videolan.vlcbenchmark.NOTIFICATION_ID"
    const val VALUE_TIME_LIMIT = 30000L
    const val EXTRA_BENCHMARK_TEST_NUMBER = "testNumber"
    const val EXTRA_RESULT_PAGE_NAME = "extra_result_page_name"
    const val SHARED_PREFERENCE_MODEL = "org.videolan.vlcbenchmark.MODEL"

    object RequestCodes {
        @JvmField
        var RESULTS = 1
        @JvmField
        var VLC = 2
        @JvmField
        var GOOGLE_CONNECTION = 3
        @JvmField
        var OPENGL = 4
        @JvmField
        var SCREENSHOT = 5
        var WEBSITE = 6
    }

    object ResultCodes {
        const val RESULT_OK = -1
        const val RESULT_CANCELED = 0
        const val RESULT_NO_HW = 1
        const val RESULT_CONNECTION_FAILED = 2
        const val RESULT_PLAYBACK_ERROR = 3
        const val RESULT_HARDWARE_ACCELERATION_ERROR = 4
        const val RESULT_VIDEO_TRACK_LOST = 5
        const val RESULT_VLC_CRASH = 6
    }

    enum class TestType {
        QUALITY, PLAYBACK, SPEED, UNKNOWN
    }

    object Extras {
        const val BENCHMARK = "extra_benchmark"
        const val FROM_START = "from_start"
        const val ACTION = "extra_benchmark_action"
        const val HARDWARE = "extra_benchmark_disable_hardware"
        const val STACKTRACE_FILE = "extra_stacktrace_file"
        const val TIMESTAMPS = "extra_benchmark_timestamps"
        const val ACTION_QUALITY = "extra_benchmark_action_quality"
        const val ACTION_PLAYBACK = "extra_benchmark_action_playback"
        const val ACTION_SPEED = "extra_benchmark_action_speed"
        const val SCREENSHOT_DIR = "extra_benchmark_screenshot_dir"
        const val TIME_LIMIT = "extra_benchmark_time_limit"
    }

    enum class ResultType {
        UNKNOWN, QUALITY, PLAYBACK, SPEED
    }
}