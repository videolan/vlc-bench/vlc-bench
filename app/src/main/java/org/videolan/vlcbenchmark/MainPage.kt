/*
 *****************************************************************************
 * MainPage.kt
 *****************************************************************************
 * Copyright © 2016-2021 VLC authors and VideoLAN
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/
package org.videolan.vlcbenchmark

import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.Menu
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.videolan.vlcbenchmark.tools.StorageManager.setStoragePreference
import org.videolan.vlcbenchmark.tools.Util.isAndroidTV
import org.videolan.vlcbenchmark.tools.SystemPropertiesProxy

const val MENU_ITEM_ID = "MENU_ITEM_ID"
open class MainPage : AppCompatActivity() {
    private lateinit var toolbar: Toolbar
    private var menuItemId = 0
    private var currentPageFragment: Fragment? = null
    private lateinit var bottomNavigationView: BottomNavigationView
    private var navigationBackTop = false
    private var navigationBarFocus = false
    private var keyEvent: KeyEvent? = null

    /* TV input handling */
    private var navigationIndex = 0
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        //Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.toolbar, menu)
        return true
    }

    private fun setCurrentFragment(itemId: Int): Boolean {
        val fragment: Fragment
        if (findViewById<View?>(R.id.main_page_fragment_holder) != null) {
            when (itemId) {
                R.id.home_nav -> {
                    if (currentPageFragment is MainPageFragment) return true
                    fragment = MainPageFragment()
                    toolbar.title = resources.getString(R.string.app_name)
                }
                R.id.results_nav -> {
                    if (currentPageFragment is MainPageResultListFragment) return true
                    fragment = MainPageResultListFragment()
                    toolbar.title = resources.getString(R.string.results_page)
                }
                R.id.settings_nav -> {
                    if (currentPageFragment is SettingsFragment) return true
                    fragment = SettingsFragment()
                    toolbar.title = resources.getString(R.string.settings_page)
                }
                else -> return false
            }
            supportFragmentManager.beginTransaction()
                    .replace(R.id.main_page_fragment_holder, fragment)
                    .commit()
            menuItemId = itemId
            currentPageFragment = fragment
        }
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_page)
        toolbar = findViewById(R.id.main_toolbar)
        setSupportActionBar(toolbar)
        setStoragePreference(this)
        val context = this
        lifecycleScope.launch(Dispatchers.IO) {
            val properName = SystemPropertiesProxy.getProperDeviceName(context)
            withContext(Dispatchers.Main) {
                currentPageFragment?.let {
                    if (it is MainPageFragment)
                        it.setDeviceName(properName)
                }
            }
        }
        val bottomNav = findViewById<BottomNavigationView>(R.id.bottom_navigation_bar)
        if (!isAndroidTV(this)) {
            bottomNav.setOnNavigationItemSelectedListener {
                item ->
                menuItemId = item.itemId
                setCurrentFragment(item.itemId) }
        }
        if (savedInstanceState == null) {
            setCurrentFragment(R.id.home_nav)
            if (isAndroidTV(this)) {
                navigationIndex = 0
                bottomNav.selectedItemId = R.id.home_nav
                bottomNav.itemBackgroundResource = R.drawable.bottom_navigation_view_item_background_tv
            }
        } else {
            menuItemId = savedInstanceState.getInt("MENU_ITEM_ID")
            setCurrentFragment(menuItemId)
            bottomNav.selectedItemId = menuItemId
        }
        bottomNavigationView = bottomNav
    }

    /**
     * dispatchKeyEvent is an override to integrate the bottomNavigationView in
     * the input flow on AndroidTV and allow to interact with it. It isn't handled natively.
     * @param event remote control key event
     * @return boolean event consumed
     */
    override fun dispatchKeyEvent(event: KeyEvent): Boolean {
        val focus = currentFocus
        val ret = super.dispatchKeyEvent(event)
        if (focus == null) {
            Log.e(TAG, "Failed to get current focus")
            return false
        }

        if (focus.id == R.id.bottom_navigation_bar) {
            navigationBarFocus = true
        } else {
            bottomNavigationView.itemBackgroundResource = R.drawable.bottom_navigation_view_item_background
        }

        if (event.action == KeyEvent.ACTION_UP) {
            if (keyEvent == null) {
                when (event.keyCode) {
                    KeyEvent.KEYCODE_DPAD_RIGHT,
                    KeyEvent.KEYCODE_MEDIA_FAST_FORWARD,
                    KeyEvent.KEYCODE_DPAD_LEFT,
                    KeyEvent.KEYCODE_MEDIA_REWIND,
                    KeyEvent.KEYCODE_DPAD_UP,
                    KeyEvent.KEYCODE_DPAD_DOWN -> keyEvent = event
                }
            }
            /* When bottom_navigation_bar gets focus, we use the inputs left and right to move
            * inside an array of fragments id. These fragments are the three principal home, results, settings,
            * updating the current fragment as we go along.
            */
            if (focus.id == R.id.bottom_navigation_bar) {
                bottomNavigationView.itemBackgroundResource = R.drawable.bottom_navigation_view_item_background_tv
                when (event.keyCode) {
                    KeyEvent.KEYCODE_DPAD_RIGHT, KeyEvent.KEYCODE_MEDIA_FAST_FORWARD -> if (navigationIndex + 1 >= 0 && navigationIndex + 1 < navigationIds.size) {
                        navigationIndex += 1
                    }
                    KeyEvent.KEYCODE_DPAD_LEFT, KeyEvent.KEYCODE_MEDIA_REWIND -> if (navigationIndex - 1 >= 0 && navigationIndex - 1 < navigationIds.size) {
                        navigationIndex -= 1
                    }
                    else -> {
                    }
                }
                bottomNavigationView.selectedItemId = navigationIds[navigationIndex]
                setCurrentFragment(navigationIds[navigationIndex])
            } else {
                when (event.keyCode) {
                    KeyEvent.KEYCODE_BACK -> if (!navigationBackTop) {
                        // Back to navbar when pressing back in a main page fragment
                        bottomNavigationView.selectedItemId = navigationIds[navigationIndex]
                        bottomNavigationView.itemBackgroundResource =
                            R.drawable.bottom_navigation_view_item_background_tv
                        bottomNavigationView.requestFocus()
                    } else {
                        navigationBackTop = false
                    }
                    KeyEvent.KEYCODE_DPAD_UP -> if (navigationBarFocus) {
                        navigationBarFocus = false
                        // if there is no results in MainPageResultListFragment, then the user
                        // shouldn't be able to focus the fragment.
                        if (currentPageFragment is MainPageResultListFragment) {
                            if ((currentPageFragment as MainPageResultListFragment).isEmpty) {
                                bottomNavigationView.selectedItemId = navigationIds[navigationIndex]
                                bottomNavigationView.itemBackgroundResource =
                                    R.drawable.bottom_navigation_view_item_background_tv
                                bottomNavigationView.requestFocus()
                            } else {
                                (currentPageFragment as MainPageResultListFragment).scrollingBackToTop()
                                return true
                            }
                        } else if (currentPageFragment is SettingsFragment) {
                            (currentPageFragment as SettingsFragment).scrollingBackToTop()
                        }
                    }
                    else -> {
                    }
                }
            }
        }
        return ret
    }

    override fun onBackPressed() {
        val focus = currentFocus
        // Catch back pressed when in a main page fragment on tv to return to the navbar instead
        // The return to navbar is handled above in dispatchKeyEvent
        if (focus != null && focus.id != R.id.bottom_navigation_bar) {
            if (currentPageFragment is MainPageResultListFragment &&
                    !(currentPageFragment as MainPageResultListFragment).isScrollingPositionTop &&
                    !navigationBackTop) {
                (currentPageFragment as MainPageResultListFragment).scrollingBackToTop()
                navigationBackTop = true
            }
            return
        }
        super.onBackPressed()
    }

    public override fun onSaveInstanceState(savedInstanceState: Bundle) {
        savedInstanceState.putInt(MENU_ITEM_ID, menuItemId)
        super.onSaveInstanceState(savedInstanceState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        menuItemId = savedInstanceState.getInt(MENU_ITEM_ID)
        super.onRestoreInstanceState(savedInstanceState)
    }

    companion object {
        private val TAG = MainPage::class.java.name
        private val navigationIds = intArrayOf(R.id.home_nav, R.id.results_nav, R.id.settings_nav)
    }
}